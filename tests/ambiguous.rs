use earley::*;

#[test]
fn some_ambiguous_grammars() {
    {
        let g = Grammar::<char, String, String>::new(
            vec![
                ("P", vec![n("S")]),
                ("S", vec![n("T")]),
                ("S", vec![]),
                ("T", vec![n("S")]),
            ],
            "P",
            vec![],
        );
        assert!(g.number_of_parses(&"".chars().collect::<Vec<_>>()) >= 1); // not sure how many parses we really want
        assert!(g.number_of_parses(&"x".chars().collect::<Vec<_>>()) == 0);
    }
    {
        let g = Grammar::<char, String, String>::new(
            vec![
                ("P", vec![n("S")]),
                ("S", vec![n("S"), t('s')]),
                ("S", vec![]),
            ],
            "P",
            vec![],
        );
        assert!(g.number_of_parses(&"".chars().collect::<Vec<_>>()) == 1);
        assert!(g.number_of_parses(&"s".chars().collect::<Vec<_>>()) == 1);
        assert!(g.number_of_parses(&"ss".chars().collect::<Vec<_>>()) == 1);
        assert!(g.number_of_parses(&"sss".chars().collect::<Vec<_>>()) == 1);
    }
    {
        let g = Grammar::<char, String, String>::new(
            vec![
                ("P", vec![n("S")]),
                ("S", vec![n("S"), t('s')]),
                ("S", vec![t('s'), n("S")]),
                ("S", vec![]),
            ],
            "P",
            vec![],
        );
        assert!(g.number_of_parses(&"".chars().collect::<Vec<_>>()) == 1);
        assert!(g.number_of_parses(&"s".chars().collect::<Vec<_>>()) == 2);
        assert!(g.number_of_parses(&"ss".chars().collect::<Vec<_>>()) == 4);
    }
}
