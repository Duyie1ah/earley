use earley::*;

// This one fails!
#[test]
fn alternating_nullable() {
    let g = Grammar::<char, String, String>::new(
        vec![("A", vec![]), ("A", vec![n("B")]), ("B", vec![n("A")])],
        "A",
        vec![],
    );
    g.recognize(&"".chars().collect::<Vec<_>>());
}

#[test]
fn pep_example() {
    // Example in “Practical Earley Parsers”
    let g = Grammar::<char, String, String>::new(
        vec![
            ("S'", vec![n("S")]),
            ("S", vec![n("A"), n("A"), n("A"), n("A")]),
            ("A", vec![t('a')]),
            ("A", vec![n("E")]),
            ("E", vec![]),
        ],
        "S'",
        vec![],
    );
    assert!(g.recognize(&"a".chars().collect::<Vec<_>>()));
}

#[test]
fn multiple_null_parses() {
    let g = Grammar::<char, String, String>::new(
        vec![
            ("P", vec![n("A")]),
            ("A", vec![n("B")]),
            ("A", vec![n("C")]),
            ("A", vec![n("D")]),
            ("B", vec![]),
            ("C", vec![]),
            ("D", vec![]),
        ],
        "P",
        vec![],
    );
    assert_eq!(g.number_of_parses(&"".chars().collect::<Vec<_>>()), 3);

    let g = Grammar::<char, String, String>::new(
        vec![
            ("P", vec![n("A")]),
            ("A", vec![n("B")]),
            ("A", vec![n("C")]),
            ("A", vec![n("D")]),
            ("B", vec![]),
            ("C", vec![]),
            ("D", vec![n("E"), n("F")]),
            ("E", vec![]),
            ("F", vec![]),
        ],
        "P",
        vec![],
    );
    assert_eq!(g.number_of_parses(&"".chars().collect::<Vec<_>>()), 3);

    let g = Grammar::<char, String, String>::new(
        vec![
            ("P", vec![n("A")]),
            ("A", vec![n("B")]),
            ("A", vec![n("C")]),
            ("A", vec![n("D")]),
            ("B", vec![]),
            ("C", vec![]),
            ("D", vec![n("E"), n("F")]),
            ("E", vec![]),
            ("F", vec![n("G")]),
            ("F", vec![n("H")]),
            ("G", vec![]),
            ("H", vec![]),
        ],
        "P",
        vec![],
    );
    assert_eq!(g.number_of_parses(&"".chars().collect::<Vec<_>>()), 4);

    // The same as above, but with rules permuted
    let g = Grammar::<char, String, String>::new(
        vec![
            ("C", vec![]),
            ("F", vec![n("H")]),
            ("D", vec![n("E"), n("F")]),
            ("E", vec![]),
            ("F", vec![n("G")]),
            ("G", vec![]),
            ("B", vec![]),
            ("H", vec![]),
            ("A", vec![n("B")]),
            ("A", vec![n("C")]),
            ("A", vec![n("D")]),
            ("P", vec![n("A")]),
        ],
        "P",
        vec![],
    );
    assert_eq!(g.number_of_parses(&"".chars().collect::<Vec<_>>()), 4);
}
