use earley::*;

#[test]
fn test_arithmetic_expressions() {
    let g = Grammar::<char, String, String>::new(
        vec![
            ("P", vec![n("e5")]),
            ("digit", vec![p("digit")]),
            ("nat", vec![n("digit")]),
            ("nat", vec![n("nat"), n("digit")]),
            ("e0", vec![n("nat")]),
            ("e1", vec![n("e0")]),
            ("e2", vec![n("e1")]),
            ("e3", vec![n("e2")]),
            ("e4", vec![n("e3")]),
            ("e5", vec![n("e4")]),
            ("e0", vec![t('('), n("e5"), t(')')]),
            ("e1", vec![n("e1"), t('!')]),
            ("e2", vec![t('-'), n("e2")]),
            ("e3", vec![n("e3"), t('*'), n("e2")]),
            ("e4", vec![n("e4"), t('+'), n("e3")]),
            ("e4", vec![n("e4"), t('-'), n("e3")]),
            //("T",vec![t('('),n("S"),t(')')]),
        ],
        "P",
        vec![(
            "digit".to_owned(),
            Box::new(|x: &char| x.is_digit(10)) as Box<dyn Fn(&char) -> bool>,
        )],
    );

    fn fac(n: i32) -> i32 {
        if n == 0 {
            1
        } else {
            n * fac(n - 1)
        }
    }

    let f = |m: &String, n: usize, v: Vec<Either<i32, &char>>| -> i32 {
        use Either::*;
        match (m.as_str(), n, &v[..]) {
            ("P", _, [L(n)]) => *n,
            ("digit", 0, [R(c)]) => c.to_digit(10).unwrap() as i32,
            ("nat", 0, [L(d)]) => *d,
            ("nat", 1, [L(n), L(d)]) => 10 * n + d,

            ("e0", 0, [L(n)]) => *n,
            ("e1", 0, [L(n)]) => *n,
            ("e2", 0, [L(n)]) => *n,
            ("e3", 0, [L(n)]) => *n,
            ("e4", 0, [L(n)]) => *n,
            ("e5", 0, [L(n)]) => *n,

            ("e0", 1, [_, L(n), _]) => *n,
            ("e1", 1, [L(n), R('!')]) if *n >= 0 => fac(*n),
            ("e2", 1, [R('-'), L(n)]) => -*n,
            ("e3", 1, [L(m), R('*'), L(n)]) => m * n,
            ("e4", 1, [L(m), R('+'), L(n)]) => m + n,
            ("e4", 2, [L(m), R('-'), L(n)]) => m - n,

            _ => {
                panic!("{:?} and {:?} and {:?}", m, n, v);
            }
        }
    };

    let exprs_res = vec![
        ("--1", 1),
        ("-3!", -6),
        ("5+-2", 3),
        ("10-10*10", -90),
        ("10*10-10", 90),
        ("3!+2!+1!", 9),
        ("(--5)!-2-((((((((((2))))))))))", 116),
        ("5-5-5", -5),
        ("5-(5-5)", 5),
    ];
    for (string, res) in exprs_res {
        assert_eq!(g.number_of_parses(&string.chars().collect::<Vec<_>>()), 1);
        assert_eq!(g.parse_fn(&string.chars().collect::<Vec<_>>(), &f)[0], res);
    }
}
