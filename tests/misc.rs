use earley::*;

#[test]
fn unhappy_peg() {
    // An example making PEG unhappy I believe
    let g = Grammar::<char, String, String>::new(
        vec![
            ("P", vec![n("S")]),
            ("S", vec![t('a'), n("S"), t('a')]),
            ("S", vec![]),
        ],
        "P",
        vec![],
    );
    assert!(g.recognize(&"".chars().collect::<Vec<_>>()));
    assert!(g.recognize(&"aa".chars().collect::<Vec<_>>()));
    assert!(g.recognize(&"aaaa".chars().collect::<Vec<_>>()));
    assert!(g.recognize(&"aaaaaa".chars().collect::<Vec<_>>()));
    assert!(!g.recognize(&"a".chars().collect::<Vec<_>>()));
}

#[test]
fn wikipedia() {
    // Example on wikipedia https://en.wikipedia.org/wiki/Earley_parser#See_also
    let g = Grammar::<char, String, String>::new(
        vec![
            ("P", vec![n("S")]),
            ("S", vec![n("S"), t('+'), n("M")]),
            ("S", vec![n("M")]),
            ("M", vec![n("M"), t('*'), n("T")]),
            ("M", vec![n("T")]),
            ("T", vec![p("digit")]),
        ],
        "P",
        vec![(
            "digit".to_owned(),
            Box::new(|x: &char| x.is_digit(10)) as Box<dyn Fn(&char) -> bool>,
        )],
    );
    assert!(g.recognize(&"2+3*4".chars().collect::<Vec<_>>()));
    assert!(g.recognize(&"2*3+4".chars().collect::<Vec<_>>()));
    assert!(!g.recognize(&"*".chars().collect::<Vec<_>>()));
    assert!(!g.recognize(&"2*".chars().collect::<Vec<_>>()));
    assert!(!g.recognize(&"-2".chars().collect::<Vec<_>>()));
}
