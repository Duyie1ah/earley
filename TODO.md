* Error reporting: 
  When a token read yields an empty Earley chart, report an error.
  List all most advanced Earley items, and the tokens they would have accepted.
  This should provide a kind of “token `x` found, but expected `y`”.
* Token position management:
  So that error reports (and more) can use this info.
  The position should be fed to the “parsing function” e.g. in `parse_fn` too, so that it can be used further down the line.
* Enable “online parsing“:
  If the input is fed slowly, the parser should work on the partial inputs without having everything.
  This should be easy.
* Enable “online `parse_fn`”:
  So that the “parsing function” could run directly: as soon as an Earley item is completed, apply the function.
* General grammars:
  By reduction to ones in BNF.
* Support of ABNF:
  To facilitate writing grammars and using them in real-life cases.
* Macros to write grammars.
  Should enable some form like:
  `
    grammar!(
      X => X | Y Z | (A B C)*(..9),
      Y => X | A | 'y'
    )
  `
  etc.
* Change `Node::PredTerm(:Fn() -> …,:PN)` to `Node::PredTerm(:PN)` and keep a dictionary `PN -> (Fn() -> …)` inside the grammar instead.
  This makes way more sense.
