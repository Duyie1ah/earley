#![feature(inherent_associated_types)]

use std::collections::HashMap;
use std::collections::HashSet;
use std::ops::Add;
use std::ops::BitOr;
use std::ops::Mul;
use std::ops::RangeBounds;
use std::ops::{Range, RangeFrom, RangeFull, RangeInclusive, RangeTo, RangeToInclusive};
use std::rc::Rc;

use crate::bnf::Grammar as FlatGrammar;
use crate::bnf::Node as FlatNode;
use crate::bnf::Rule as FlatRule;
use crate::util::*;

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum Node<T, N, PN> {
    NonTerm(N),
    Terminal(T),
    PredTerm(PN),
    Alternates(Vec<RcNode<T, N, PN>>),
    Sequence(Vec<RcNode<T, N, PN>>),
    Repetition(RcNode<T, N, PN>, usize, Option<usize>),
}

pub fn t(t: char) -> RcNode<char, String, String> {
    RcNode::new(Node::Terminal(t))
}
pub fn s(s: &str) -> RcNode<char, String, String> {
    RcNode::new(Node::Sequence(s.chars().map(t).collect()))
}
pub fn n(n: &str) -> RcNode<char, String, String> {
    RcNode::new(Node::NonTerm(n.to_owned()))
}
pub fn p(n: &str) -> RcNode<char, String, String> {
    RcNode::new(Node::PredTerm(n.to_owned()))
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct RcNode<T, N, PN> {
    pub x: Rc<Node<T, N, PN>>,
}

impl<T, N, P> RcNode<T, N, P> {
    fn new(x: Node<T, N, P>) -> Self {
        RcNode { x: Rc::new(x) }
    }
    fn clone(&self) -> Self {
        RcNode {
            x: Rc::clone(&self.x),
        }
    }
}

impl<T, N, P> Mul<Range<usize>> for RcNode<T, N, P>
where
    T: Clone,
    N: Clone,
    P: Clone,
{
    type Output = RcNode<T, N, P>;

    fn mul(self, rhs: Range<usize>) -> Self::Output {
        RcNode::new(Node::Repetition(self.clone(), rhs.start, Some(rhs.end)))
    }
}
impl<T, N, P> Mul<RangeFrom<usize>> for RcNode<T, N, P>
where
    T: Clone,
    N: Clone,
    P: Clone,
{
    type Output = RcNode<T, N, P>;

    fn mul(self, rhs: RangeFrom<usize>) -> Self::Output {
        RcNode::new(Node::Repetition(self.clone(), rhs.start, None))
    }
}
impl<T, N, P> Mul<RangeFull> for RcNode<T, N, P>
where
    T: Clone,
    N: Clone,
    P: Clone,
{
    type Output = RcNode<T, N, P>;

    fn mul(self, rhs: RangeFull) -> Self::Output {
        RcNode::new(Node::Repetition(self.clone(), 0, None))
    }
}

impl<T, N, P> Mul<RangeTo<usize>> for RcNode<T, N, P>
where
    T: Clone,
    N: Clone,
    P: Clone,
{
    type Output = RcNode<T, N, P>;

    fn mul(self, rhs: RangeTo<usize>) -> Self::Output {
        RcNode::new(Node::Repetition(self.clone(), 0, Some(rhs.end)))
    }
}
impl<T, N, P> Mul<usize> for RcNode<T, N, P>
where
    T: Clone,
    N: Clone,
    P: Clone,
{
    type Output = RcNode<T, N, P>;

    fn mul(self, rhs: usize) -> Self::Output {
        RcNode::new(Node::Repetition(self.clone(), rhs, Some(rhs)))
    }
}

impl<T, N, P> Add<RcNode<T, N, P>> for RcNode<T, N, P>
where
    T: Clone,
    N: Clone,
    P: Clone,
{
    type Output = RcNode<T, N, P>;

    fn add(self, rhs: RcNode<T, N, P>) -> Self::Output {
        use Node::*;
        let mut v: Vec<RcNode<T, N, P>> = vec![];
        match self.x.as_ref() {
            Sequence(left) => v.extend(left.clone().into_iter()),
            _ => v.push(self.clone()),
        };
        match rhs.x.as_ref() {
            Sequence(right) => v.extend(right.clone().into_iter()),
            _ => v.push(rhs.clone()),
        };
        return RcNode::new(Sequence(v));
    }
}

impl<T, N, P> BitOr<RcNode<T, N, P>> for RcNode<T, N, P>
where
    T: Clone,
    N: Clone,
    P: Clone,
{
    type Output = RcNode<T, N, P>;

    fn bitor(self, rhs: RcNode<T, N, P>) -> Self::Output {
        use Node::*;
        let mut v: Vec<RcNode<T, N, P>> = vec![];
        match self.x.as_ref() {
            Alternates(left) => v.extend(left.clone().into_iter()),
            _ => v.push(self.clone()),
        };
        match rhs.x.as_ref() {
            Alternates(right) => v.extend(right.clone().into_iter()),
            _ => v.push(rhs.clone()),
        };
        return RcNode::new(Alternates(v));
    }
}

type FName<T, N, PN> = Either<N, RcNode<T, N, PN>>;
pub struct Grammar<T, N, PN> {
    pub augmented_rules: HashMap<N, RcNode<T, N, PN>>,
    pub flattened_grammar: FlatGrammar<T, FName<T, N, PN>, PN>,
}

impl<T, N, PN> Grammar<T, N, PN>
where
    T: std::hash::Hash + Eq + Clone + std::fmt::Debug,
    N: std::hash::Hash + Eq + Clone + std::fmt::Debug,
    PN: std::hash::Hash + Eq + Clone + std::fmt::Debug,
{
    fn recursive_node_to_flatrules(
        node: RcNode<T, N, PN>,
        flatrules: &mut Vec<(FName<T, N, PN>, Vec<FlatNode<T, FName<T, N, PN>, PN>>)>,
        seen: &mut HashMap<RcNode<T, N, PN>, FlatNode<T, FName<T, N, PN>, PN>>,
    ) -> FlatNode<T, FName<T, N, PN>, PN> {
        match seen.get(&node) {
            Some(v) => v.clone(),
            None => {
                let my_temporary_name = Either::R(node.clone());
                let mut my_rules = vec![];
                let fnode = match node.x.as_ref() {
                    Node::Terminal(t) => FlatNode::Terminal(t.clone()),
                    Node::PredTerm(p) => FlatNode::PredTerm(p.clone()),
                    Node::NonTerm(n) => FlatNode::NonTerm(Either::L(n.clone())),
                    Node::Sequence(vv) => {
                        my_rules.push((my_temporary_name.clone(), vec![]));
                        for v in vv {
                            let u =
                                Grammar::recursive_node_to_flatrules(v.clone(), flatrules, seen);
                            my_rules[0].1.push(u);
                        }
                        FlatNode::NonTerm(my_temporary_name)
                    }
                    Node::Alternates(vv) => {
                        for v in vv {
                            let u =
                                Grammar::recursive_node_to_flatrules(v.clone(), flatrules, seen);
                            my_rules.push((my_temporary_name.clone(), vec![u]));
                        }
                        FlatNode::NonTerm(my_temporary_name)
                    }
                    Node::Repetition(v, lower, Some(upper)) => {
                        let u = Grammar::recursive_node_to_flatrules(v.clone(), flatrules, seen);
                        for i in *lower..=*upper {
                            my_rules.push((my_temporary_name.clone(), vec![u.clone(); i]));
                        }
                        FlatNode::NonTerm(my_temporary_name)
                    }
                    Node::Repetition(v, lower, None) => {
                        let u = Grammar::recursive_node_to_flatrules(v.clone(), flatrules, seen);
                        my_rules.push((my_temporary_name.clone(), vec![u.clone(); *lower]));
                        my_rules.push((
                            my_temporary_name.clone(),
                            vec![FlatNode::NonTerm(my_temporary_name.clone()), u.clone()],
                        ));
                        FlatNode::NonTerm(my_temporary_name)
                    }
                };
                flatrules.extend(my_rules);
                seen.insert(node.clone(), fnode.clone());
                fnode
            }
        }
    }

    fn new(
        rules: impl IntoIterator<Item = (N, RcNode<T, N, PN>)> + Clone,
        start: N,
        preds: impl IntoIterator<Item = (PN, Box<dyn Fn(&T) -> bool>)>,
    ) -> Self {
        let original_rules: HashMap<_, _> = rules.clone().into_iter().collect();
        let mut seen: HashMap<_, _> = HashMap::new();
        let mut flattened_rules: Vec<(FName<T, N, PN>, Vec<FlatNode<T, FName<T, N, PN>, PN>>)> =
            Vec::new();
        for (name, tree) in &original_rules {
            let node =
                Grammar::recursive_node_to_flatrules(tree.clone(), &mut flattened_rules, &mut seen);
            flattened_rules.push((Either::L(name.clone()), vec![node]));
        }

        Grammar {
            augmented_rules: original_rules,
            flattened_grammar: FlatGrammar::new_from_iters(
                flattened_rules,
                Either::L(start.clone()),
                preds,
            ),
        }
    }

    pub fn recognize<'a, 'b>(&'a self, stream: impl IntoIterator<Item = &'b T>) -> bool
    where
        T: 'b,
        N: Clone + std::fmt::Debug,
    {
        self.flattened_grammar.recognize(stream)
    }
}

#[derive(Debug, Clone)]
pub enum ParseNode<'a, T, R> {
    Res(R),
    Tok(&'a T),
    Seq(Vec<ParseNode<'a, T, R>>),
    Alt(usize, Box<ParseNode<'a, T, R>>),
    Rep(Vec<ParseNode<'a, T, R>>),
}

impl<'a, T, R> ParseNode<'a, T, R> {
    pub fn get_res(self) -> R {
        match self {
            ParseNode::Res(r) => r,
            _ => panic!(),
        }
    }
}

impl<T, N, PN> Grammar<T, N, PN>
where
    T: std::hash::Hash + Eq + Clone + std::fmt::Debug,
    N: std::hash::Hash + Eq + Clone + std::fmt::Debug,
    PN: std::hash::Hash + Eq + Clone + std::fmt::Debug,
{
    pub fn parse_fn<'a, 'b, P>(
        &'a self,
        stream: impl IntoIterator<Item = &'b T>,
        f: &(impl Fn(&N, ParseNode<'b, T, P>) -> P),
    ) -> Vec<P>
    where
        T: 'b,
        P: Clone + std::fmt::Debug + 'a + 'b,
        N: Clone + std::fmt::Debug,
    {
        use Either::*;
        use Node::*;
        use ParseNode::*;

        let earley_f = |n: &Either<N, RcNode<T, N, PN>>,
                        i: usize,
                        vv: Vec<Either<ParseNode<'b, T, P>, &'b T>>|
         -> ParseNode<'b, T, P> {
            let vv: Vec<ParseNode<'b, T, P>> = vv
                .into_iter()
                .map(|x| match x {
                    L(val) => val,
                    R(t) => Tok(t),
                })
                .collect();

            match (n, i, &vv[..]) {
                (L(name), 0, [v]) => Res(f(&name, *v)),
                (R(rc), _, _) => match (rc.x.as_ref(), i, &vv[..]) {
                    (Terminal(_), _, _) | (PredTerm(_), _, _) | (NonTerm(_), _, _) => {
                        panic!("these shouldn't appear I think")
                    }
                    (Sequence(_), 0, _) => Seq(vv),
                    (Alternates(_), i, [v]) => Alt(i, Box::new(*v)),
                    (Repetition(_, _, None), 0, _) => Rep(vv),
                    (Repetition(_, _, None), 1, [Rep(ww), v]) => Rep({
                        let mut zz = ww.clone();
                        zz.push(*v);
                        zz
                    }),
                    (Repetition(_, _, Some(_)), _, _) => Rep(vv),
                    _ => {
                        panic!("I'm confused");
                    }
                },
                (_, _, _) => {
                    panic!("I'm confused");
                }
            }
        };

        self.flattened_grammar
            .parse_fn(stream, &earley_f)
            .into_iter()
            .map(|x| match x {
                Res(r) => r,
                _ => {
                    panic!("unexpected");
                }
            })
            .collect()
    }
}

mod test {
    use super::*;

    #[test]
    fn test_parse_fn() {
        let balanced = (t('(') + n("balanced") + t(')') + n("balanced")) | s("");
        let g = Grammar::new(
            vec![("balanced".to_owned(), balanced)],
            "balanced".to_owned(),
            vec![],
        );
        assert!(g.recognize(&vec![]));
        assert!(g.recognize(&vec!['(', ')']));
        assert!(g.recognize(&vec!['(', ')', '(', ')']));
        assert!(g.recognize(&vec!['(', '(', ')', ')']));
        assert!(!g.recognize(&vec![')', ')', '(', ')']));

        fn max_height(name: &String, node: ParseNode<char, usize>) -> usize {
            match (name.as_str(), node) {
                ("balanced", ParseNode::Alt(1, _)) => 0,
                ("balanced", ParseNode::Alt(0, xx)) => match *xx {
                    ParseNode::Seq(v) => {
                        assert!(v.len() == 4);
                        std::cmp::max(v[1].clone().get_res() + 1, v[3].clone().get_res())
                    }
                    _ => panic!(),
                },
                _ => {
                    panic!();
                }
            }
        }
        assert!(g.parse_fn(&vec![], &max_height)[0] == 0);
    }

    #[test]
    fn something() {
        {
            let n = t('a');
            let g = Grammar::new(vec![("A".to_owned(), n * (..))], "A".to_owned(), vec![]);

            assert!(g.recognize(vec![]));
            assert!(g.recognize(&vec!['a']));
            assert!(g.recognize(&vec!['a', 'a']));
        }
        {
            let a = RcNode::new(Node::<char, String, String>::Terminal('a'));
            //let b = RcNode::new(Node::<char, String, String>::Terminal('b'));
            //let ab = a.clone() | b.clone();
            let C = RcNode::new(Node::<char, String, String>::NonTerm("C".to_owned()));
            let g = Grammar::new(
                vec![("C".to_owned(), (C.clone() + a.clone()) | a.clone())],
                "C".to_owned(),
                vec![],
            );

            assert!(g.recognize(&vec!['a']));
            assert!(g.recognize(&vec!['a', 'a']));

            assert!(!g.recognize(&vec![]));
            assert!(!g.recognize(&vec!['b']));
        }
        {
            let a = RcNode::new(Node::<char, String, String>::Terminal('a'));
            let g = Grammar::new(vec![("C".to_owned(), a * (1..2))], "C".to_owned(), vec![]);

            assert!(g.recognize(&vec!['a']));
            assert!(g.recognize(&vec!['a', 'a']));
            assert!(!g.recognize(&vec![]));
            assert!(!g.recognize(&vec!['a', 'a', 'a']));
            assert!(!g.recognize(&vec!['b']));
        }
        {
            let a = RcNode::new(Node::<char, String, String>::Terminal('a'));
            let g = Grammar::new(vec![("C".to_owned(), a * (1..))], "C".to_owned(), vec![]);
            assert!(g.recognize(&vec!['a']));
            assert!(g.recognize(&vec!['a', 'a']));
            assert!(g.recognize(&vec!['a', 'a', 'a']));
            assert!(!g.recognize(&vec![]));
            assert!(!g.recognize(&vec!['b']));
        }
        {
            let a = RcNode::new(Node::<char, String, String>::Terminal('a'));
            let g = Grammar::new(vec![("C".to_owned(), a * (0..))], "C".to_owned(), vec![]);

            assert!(g.recognize(&vec![]));
            assert!(g.recognize(&vec!['a']));
            assert!(g.recognize(&vec!['a', 'a']));
            assert!(g.recognize(&vec!['a', 'a', 'a']));
            assert!(!g.recognize(&vec!['b']));
        }
        {
            let a = RcNode::new(Node::<char, String, String>::Terminal('a'));
            let b = RcNode::new(Node::<char, String, String>::Terminal('b'));
            let c = RcNode::new(Node::<char, String, String>::Terminal('c'));
            let d = RcNode::new(Node::<char, String, String>::Terminal('d'));
            let g = Grammar::new(
                vec![("C".to_owned(), a + b + c + d)],
                "C".to_owned(),
                vec![],
            );

            assert!(g.recognize(&vec!['a', 'b', 'c', 'd']));
            assert!(!g.recognize(&vec!['a', 'b', 'd', 'c']));
        }
        {
            let a = RcNode::new(Node::<char, String, String>::Terminal('a'));
            let b = RcNode::new(Node::<char, String, String>::Terminal('b'));
            let c = RcNode::new(Node::<char, String, String>::Terminal('c'));
            let d = RcNode::new(Node::<char, String, String>::Terminal('d'));
            let C = (a + b) | (c + d);
            let g = Grammar::new(vec![("C".to_owned(), C)], "C".to_owned(), vec![]);

            assert!(g.recognize(&vec!['a', 'b']));
            assert!(g.recognize(&vec!['c', 'd']));
            assert!(!g.recognize(&vec!['c']));
            assert!(!g.recognize(&vec!['a', 'd']));
        }

        {
            let URL = (t('s') * (0..1))
                + s("ftp://")
                + (p("alphanum") * (1..) + t('.')) * (0..)
                + (p("alphanum") * (1..))
                + (t('/') + (p("alphanum") * (1..)) * (0..) + (t('/') * (0..1))) * (0..);
            let g = Grammar::new(
                vec![("URL".to_owned(), URL)],
                "URL".to_owned(),
                vec![(
                    "alphanum".to_owned(),
                    Box::new(|x: &char| x.is_alphanumeric()) as Box<dyn Fn(&char) -> bool>,
                )],
            );
            for r in &g.flattened_grammar.rules {
                println!("{:?} -> {:?}", r.lhs, r.rhs)
            }
            //assert!(g.parse(&vec!['s', 'f', 't', 'p']));
            assert!(g.recognize(&"sftp://aa.b.c.d".chars().collect::<Vec<_>>()));
            assert!(g.recognize(&"sftp://aa.b.c.0.1".chars().collect::<Vec<_>>()));
            assert!(g.recognize(&"sftp://a".chars().collect::<Vec<_>>()));
            assert!(g.recognize(&"sftp://a/b/c/d".chars().collect::<Vec<_>>()));
            assert!(g.recognize(&"sftp://acd.ef/b/c/d".chars().collect::<Vec<_>>()));
            assert!(g.recognize(&"ftp://aa.b.c.d".chars().collect::<Vec<_>>()));
            assert!(g.recognize(&"ftp://aa.b.c.0.1".chars().collect::<Vec<_>>()));
            assert!(g.recognize(&"ftp://a".chars().collect::<Vec<_>>()));
            assert!(g.recognize(&"ftp://a/b/c/d".chars().collect::<Vec<_>>()));
            assert!(g.recognize(&"ftp://acd.ef/b/c/d".chars().collect::<Vec<_>>()));
            assert!(!g.recognize(&"sftp://a.".chars().collect::<Vec<_>>()));
            assert!(!g.recognize(&vec!['s', 't']));
        }
    }
}
