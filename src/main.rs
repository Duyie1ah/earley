use earley::{n, p, t, Either, Grammar};
use std::{io, io::prelude::*};

fn main() {
    let g = Grammar::<char, String, String>::new(
        vec![
            ("P", vec![n("S")]),
            ("S", vec![p("digit")]),
            ("S", vec![n("S"), t('-'), n("S")]),
            ("S", vec![n("S"), t('+'), n("S")]),
            //("T",vec![t('('),n("S"),t(')')]),
        ],
        "P",
        vec![(
            "digit".to_owned(),
            Box::new(|x: &char| x.is_digit(10)) as Box<dyn Fn(&char) -> bool>,
        )],
    );

    let f = |m: &String, n: usize, v: Vec<Either<i32, &char>>| -> i32 {
        use Either::*;
        match (m.as_str(), n, &v[..]) {
            ("P", _, [L(n)]) => *n,
            ("S", 0, [R(c)]) => c.to_digit(10).unwrap() as i32,
            ("S", 1, [L(m), R('-'), L(n)]) => m - n,
            ("S", 2, [L(m), R('+'), L(n)]) => m + n,
            _ => {
                panic!("{:?} and {:?} and {:?}", m, n, v);
            }
        }
    };

    for line in io::stdin().lock().lines() {
        match line {
            Ok(line) => {
                let res = g.parse_fn(
                    &line
                        .chars()
                        .filter(|x| !x.is_whitespace())
                        .collect::<Vec<_>>(),
                    &f,
                );
                if res.len() != 1 {
                    println!("Parsing failed.");
                    println!("res is {:?}", res);
                } else {
                    println!("→ {}", res[0]);
                }
            }
            Err(e) => {
                println!("WTF({})", e);
            }
        }
    }
}
