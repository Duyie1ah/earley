use std::fmt;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Either<L, R> {
    L(L),
    R(R),
}
impl<L, R> fmt::Display for Either<L, R>
where
    L: std::fmt::Display,
    R: std::fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Either::L(l) => {
                write!(f, "L({})", l)
            }
            Either::R(r) => {
                write!(f, "R({})", r)
            }
        }
    }
}
