#![feature(assert_matches)]

use std::fmt;
use std::ops::{Index, IndexMut};

use crate::bnf::*;
use crate::util::*;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct EarleyItem<'a, T, N, PN> {
    rule: &'a Rule<T, N, PN>,
    dot: usize,
    start: usize,
    current: usize,
}

impl<'a, T, N, PN> EarleyItem<'a, T, N, PN> {
    fn init(rule: &'a Rule<T, N, PN>, pos: usize) -> Self {
        EarleyItem {
            rule: rule,
            dot: 0,
            start: pos,
            current: pos,
        }
    }

    fn next(&self) -> Option<&'a Node<T, N, PN>> {
        if self.dot >= self.rule.len() {
            None
        } else {
            Some(&self.rule.rhs[self.dot])
        }
    }

    fn is_completed(&self) -> Option<(usize, usize)> {
        if self.dot >= self.rule.len() {
            Some((self.start, self.current))
        } else {
            None
        }
    }

    fn advance(&self, new_current: usize) -> Option<Self> {
        if self.dot >= self.rule.rhs.len() {
            None
        } else {
            Some(EarleyItem {
                rule: self.rule,
                dot: self.dot + 1,
                start: self.start,
                current: new_current,
            })
        }
    }

    fn is_null_parse(&self) -> bool {
        if let Some((x, y)) = self.is_completed() {
            x == y
        } else {
            false
        }
    }
}

impl<'a, T, N, PN> fmt::Display for EarleyItem<'a, T, N, PN>
where
    T: std::fmt::Display,
    N: std::fmt::Display,
    PN: std::fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} → ", self.rule.lhs)?;
        for n in &self.rule.rhs[0..self.dot] {
            write!(f, "{} ", n)?;
        }
        write!(f, "• ")?;
        for n in &self.rule.rhs[self.dot..] {
            write!(f, "{} ", n)?;
        }
        write!(f, " | {}..{}", self.start, self.current)
    }
}

type EarleyItemPtr = (usize, usize);
type EarleyItemPreds<'b, T> = Vec<(Either<EarleyItemPtr, &'b T>, EarleyItemPtr)>;
type EarleySet<'a, 'b, T, N, PN> = Vec<(EarleyItem<'a, T, N, PN>, EarleyItemPreds<'b, T>)>;

#[derive(Debug, PartialEq, Eq, Clone)]
struct EarleySets<'a, 'b, T, N, PN> {
    sets: Vec<EarleySet<'a, 'b, T, N, PN>>,
    emptyvec: EarleySet<'a, 'b, T, N, PN>,
}

impl<'a, 'b, T, N, PN> Index<usize> for EarleySets<'a, 'b, T, N, PN> {
    type Output = EarleySet<'a, 'b, T, N, PN>;

    fn index(&self, i: usize) -> &Self::Output {
        if self.sets.len() <= i {
            &self.emptyvec // ugly hack
        } else {
            &self.sets[i]
        }
    }
}
impl<'a, 'b, T, N, PN> IndexMut<usize> for EarleySets<'a, 'b, T, N, PN> {
    fn index_mut(&mut self, i: usize) -> &mut Self::Output {
        while self.sets.len() <= i {
            self.sets.push(vec![])
        }
        &mut self.sets[i]
    }
}

impl<'a, 'b, T, N, PN> EarleySets<'a, 'b, T, N, PN>
where
    T: Eq,
    N: Eq,
    PN: Eq,
{
    fn new() -> Self {
        EarleySets {
            sets: vec![],
            emptyvec: vec![],
        }
    }

    fn len(&self) -> usize {
        self.sets.len()
    }

    fn push(
        &mut self,
        item: EarleyItem<'a, T, N, PN>,
        preds: impl IntoIterator<Item = (Either<EarleyItemPtr, &'b T>, EarleyItemPtr)>,
    ) -> (bool, usize) {
        let current = item.current;
        while self.sets.len() <= current {
            self.sets.push(vec![]);
        }

        match self.sets[current]
            .iter()
            .position(|(item2, preds2)| *item2 == item)
        {
            None => {
                self.sets[current].push((item, preds.into_iter().collect()));
                (true, self.sets[current].len() - 1)
            }
            Some(p) => {
                let here = &mut self.sets[current][p].1;
                for x in preds {
                    if !here.contains(&x) {
                        here.push(x);
                    }
                }
                (false, p)
            }
        }
    }
}

impl<'a, 'b, T, N, PN> fmt::Display for EarleySets<'a, 'b, T, N, PN>
where
    T: std::fmt::Display + std::fmt::Debug,
    N: std::fmt::Display + std::fmt::Debug,
    PN: std::fmt::Display + std::fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Ok(for (i, set) in self.sets.iter().enumerate() {
            writeln!(f, "{}:", i)?;
            for (idx, state) in set.iter().enumerate() {
                writeln!(f, "  {}.   {}   … {:?}", idx, state.0, state.1)?;
            }
        })
    }
}

impl<T, N, PN> Grammar<T, N, PN>
where
    T: std::fmt::Debug + std::cmp::PartialEq + std::cmp::Eq + Clone + std::hash::Hash,
    N: std::fmt::Debug + std::cmp::PartialEq + std::cmp::Eq + Clone + std::hash::Hash,
    PN: std::fmt::Debug + std::cmp::PartialEq + std::cmp::Eq + Clone + std::hash::Hash,
{
    fn earley_sets<'a, 'b>(
        &'a self,
        name: &N,
        mut stream: impl Iterator<Item = &'b T>,
    ) -> (EarleySets<'a, 'b, T, N, PN>, usize)
    where
        T: std::hash::Hash + std::fmt::Debug + 'b,
    {
        let mut earleysets: EarleySets<'a, 'b, T, N, PN> = EarleySets::new();
        for r in self.rules.iter().filter(|r| r.lhs == *name) {
            earleysets.push(EarleyItem::<T, N, PN>::init(&r, 0), vec![]);
        }

        let null_parses = self.nullable_rules();

        let mut stream_position = 0;
        loop {
            let t = stream.next();
            let mut item_idx = 0;

            while earleysets[stream_position].len() > item_idx {
                let i = stream_position;
                let item = &earleysets[i][item_idx].0.clone();

                /*
                println!("            ");
                println!("------------");
                println!("reading    : {:?}", t);
                println!("earley_sets:");
                println!("{}", earleysets);
                println!("stream_position: {}", stream_position);
                println!("item_idx       : {}", item_idx);
                println!("item           : {}    ", item);
                */
                match item.next() {
                    None => {
                        // Completion
                        //println!("→ completion");
                        for r_idx in 0..earleysets[item.start].len() {
                            let r = &earleysets[item.start][r_idx];

                            match r.0.next() {
                                Some(Node::NonTerm(n)) if *n == item.rule.lhs => {
                                    let r_advanced = r.0.advance(i).unwrap();
                                    earleysets
                                        .push(
                                            r_advanced,
                                            Some((Either::L((i, item_idx)), (item.start, r_idx))),
                                        )
                                        .0;
                                }
                                _ => {}
                            };
                        }
                    }
                    Some(Node::NonTerm(name)) => {
                        // Prediction
                        //println!("→ prediction");
                        for r in self.rules.iter().filter(|r| r.lhs == *name) {
                            let predicted = EarleyItem::init(r, i);
                            earleysets.push(predicted, None).0;

                            // TODO: clarify why this works
                            // The original idea was simply to leave the “completed” item without
                            // predecessors, and when applying the function of `parse_fn` to the parse chart, and processing a completed null parse, jump over to the earley sets for this null parse and get their result, so we don't have to do all the “merge earley sets” business.
                            if null_parses.contains(r) {
                                let predicted_completed = EarleyItem {
                                    rule: r,
                                    start: i,
                                    current: i,
                                    dot: r.rhs.len(),
                                };
                                assert!(predicted_completed.is_null_parse());
                                let predicted_completed_idx =
                                    earleysets.push(predicted_completed, None).1;
                                earleysets.push(
                                    item.advance(i).unwrap(),
                                    Some((Either::L((i, predicted_completed_idx)), (i, item_idx))),
                                );
                            }
                        }
                    }
                    Some(Node::Terminal(t2)) => {
                        // Scanning
                        //println!("→ scanning");
                        if t == Some(t2) {
                            let item_advanced = item.advance(i + 1).unwrap();
                            earleysets
                                .push(item_advanced, Some((Either::R(t.unwrap()), (i, item_idx))))
                                .0;
                        }
                    }
                    Some(Node::PredTerm(name)) => {
                        // Scanning
                        //println!("→ scanning");
                        if t.is_some()
                            && self
                                .predicates
                                .get(&name)
                                .expect("Predicate should be defined!")(
                                t.unwrap()
                            )
                        {
                            let item_advanced = item.advance(i + 1).unwrap();
                            earleysets
                                .push(item_advanced, Some((Either::R(t.unwrap()), (i, item_idx))))
                                .0;
                        } else {
                        }
                    }
                };
                item_idx += 1;
            }

            if t == None {
                break;
            }
            stream_position += 1;
        }
        (earleysets, stream_position)
    }

    fn apply_f_rec<'a, 'b, 'c, P>(
        &'a self,
        earleysets: &'c EarleySets<'a, 'b, T, N, PN>,
        i: usize,
        item_idx: usize,
        f: &'c impl Fn(&N, usize, Vec<Either<P, &'b T>>) -> P,
        depth: usize,
        seen: &mut Vec<EarleyItemPtr>,
    ) -> impl Iterator<Item = P> + 'c
    where
        'c: 'a + 'b,
        P: Clone + std::fmt::Debug + 'c,
    {
        let item = &earleysets[i][item_idx].0;
        //println!("{}", item);
        assert!(item.is_completed().is_some());

        let preds_vecs = self.pred_vecs(earleysets, i, item_idx, f, depth, seen);

        preds_vecs.map(move |v| f(&item.rule.lhs, item.rule.idx, v))
    }

    fn pred_vecs<'a, 'b, 'c, P>(
        &'a self,
        earleysets: &'c EarleySets<'a, 'b, T, N, PN>,
        i: usize,
        item_idx: usize,
        f: &'c impl Fn(&N, usize, Vec<Either<P, &'b T>>) -> P,
        depth: usize,
        seen: &mut Vec<EarleyItemPtr>, // allows escaping infinite loop when constructing parse trees on bad (nullable) grammars
    ) -> impl Iterator<Item = Vec<Either<P, &'b T>>> + 'c
    where
        'c: 'a + 'b,
        P: Clone + std::fmt::Debug + 'c,
    {
        let preds = &earleysets[i][item_idx].1;
        if preds.len() == 0 {
            vec![vec![]].into_iter()
        } else {
            if seen.contains(&(i, item_idx)) {
                return vec![vec![]].into_iter();
            }
            seen.push((i, item_idx));
            let mut to_return: Vec<Vec<Either<P, &T>>> = Vec::new();

            for (x, (j, k)) in preds {
                let out = self.pred_vecs(earleysets, *j, *k, f, depth, seen);
                let xp = match x {
                    Either::L((m, n)) => {
                        if seen.contains(&(*m, *n)) {
                            vec![]
                        } else {
                            let res = self
                                .apply_f_rec(earleysets, *m, *n, f, depth + 1, seen)
                                .map(Either::L)
                                .collect::<Vec<Either<P, &T>>>();
                            res
                        }
                    }
                    Either::R(t) => vec![Either::R(*t)],
                };
                for o in out {
                    for x in &xp {
                        let mut oo = o.clone();
                        oo.push(x.clone());
                        to_return.push(oo);
                    }
                }
            }
            seen.pop();
            to_return.into_iter()
        }
    }

    pub fn parse_fn<'a, 'b, P>(
        &'a self,
        stream: impl IntoIterator<Item = &'b T>,
        f: &impl Fn(&N, usize, Vec<Either<P, &T>>) -> P,
    ) -> Vec<P>
    where
        T: 'b,
        P: Clone + std::fmt::Debug + 'a + 'b,
        N: Clone + std::fmt::Debug,
    {
        let (earleysets, last_stream_position) = self.earley_sets(&self.start, stream.into_iter());
        //println!("{}", earleysets);
        let len = last_stream_position;

        let res = earleysets[len]
            .iter()
            .enumerate()
            .filter(|(_idx, state)| {
                state.0.is_completed() == Some((0, len)) && state.0.rule.lhs == self.start
                // want completed items that start at 0 and end at stream.len() and that correspond to
                // the top level rule
            })
            .map(|(idx, _state)| self.apply_f_rec(&earleysets, len, idx, f, 1, &mut vec![]))
            .flatten()
            .collect::<Vec<P>>()
            .clone();
        res
    }

    pub fn number_of_parses<'a, 'b>(&'a self, stream: impl IntoIterator<Item = &'b T>) -> usize
    where
        T: 'b,
        N: Clone + std::fmt::Debug,
    {
        self.parse_fn(stream, &|_, _, _| ()).len()
    }

    pub fn recognize<'a, 'b>(&'a self, stream: impl IntoIterator<Item = &'b T>) -> bool
    where
        T: 'b,
        N: Clone + std::fmt::Debug,
    {
        self.number_of_parses(stream) > 0
    }
}
