use std::collections::{HashMap, HashSet};
use std::fmt;
use std::hash::{Hash, Hasher};

#[derive(Eq, PartialEq, Hash, Clone, Debug)]
pub enum Node<T, N, PN> {
    NonTerm(N),
    Terminal(T),
    PredTerm(PN),
}

impl<T, N, PN> fmt::Display for Node<T, N, PN>
where
    T: std::fmt::Display,
    N: std::fmt::Display,
    PN: std::fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Node::NonTerm(n) => {
                write!(f, "{}", n)
            }
            Node::Terminal(t) => {
                write!(f, "'{}'", t)
            }
            Node::PredTerm(n) => {
                write!(f, ":{}:", n)
            }
        }
    }
}

pub fn n<T, P>(n: &str) -> Node<T, String, P>
where
    T: 'static,
{
    Node::NonTerm(n.to_owned())
}
pub fn t<T, N, PN>(t: T) -> Node<T, N, PN>
where
    T: 'static,
{
    Node::Terminal(t)
}
pub fn p<T, N>(name: &str) -> Node<T, N, String>
where
    T: 'static,
{
    Node::PredTerm(name.to_owned())
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Rule<T, N, PN> {
    pub lhs: N,
    pub idx: usize,
    pub rhs: Vec<Node<T, N, PN>>,
}

impl<T, N, PN> Rule<T, N, PN> {
    pub fn len(&self) -> usize {
        self.rhs.len()
    }
}

impl<'a, T, N, PN> fmt::Display for Rule<T, N, PN>
where
    T: std::fmt::Display,
    N: std::fmt::Display,
    PN: std::fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} → ", self.lhs)?;
        Ok(for n in &self.rhs {
            write!(f, "{} ", n)?;
        })
    }
}

pub struct Grammar<T, N, PN> {
    pub rules: Vec<Rule<T, N, PN>>,
    pub predicates: HashMap<PN, Box<dyn Fn(&T) -> bool>>,
    pub start: N,
}

impl<T, N, PN> Grammar<T, N, PN>
where
    T: std::fmt::Debug + std::cmp::PartialEq + std::cmp::Eq + Clone + std::hash::Hash,
    N: std::fmt::Debug + std::cmp::PartialEq + std::cmp::Eq + Clone + std::hash::Hash,
    PN: std::fmt::Debug + std::cmp::PartialEq + std::cmp::Eq + Clone + std::hash::Hash,
{
    pub fn new(
        rules: Vec<(&str, Vec<Node<T, String, String>>)>,
        start: &str,
        preds: impl IntoIterator<Item = (String, Box<dyn Fn(&T) -> bool>)>,
    ) -> Grammar<T, String, String> {
        let mut rule_counter: HashMap<&str, usize> = rules.iter().map(|(x, y)| (*x, 0)).collect();
        let mut g = Grammar {
            rules: vec![],
            start: start.to_owned(),
            predicates: preds.into_iter().collect(),
        };
        for r in rules {
            let idx = rule_counter.get(&r.0).unwrap();
            g.rules.push(Rule {
                lhs: r.0.to_owned(),
                idx: *idx,
                rhs: r.1,
            });
            rule_counter.insert(r.0, idx + 1);
        }
        return g;
    }

    pub fn new_from_iters(
        rules: impl IntoIterator<Item = (N, Vec<Node<T, N, PN>>)> + Clone,
        start: N,
        preds: impl IntoIterator<Item = (PN, Box<dyn Fn(&T) -> bool>)>,
    ) -> Grammar<T, N, PN> {
        let mut rule_counter: HashMap<N, usize> = rules
            .clone()
            .into_iter()
            .map(|(x, y)| (x.clone(), 0))
            .collect();
        let mut g = Grammar {
            rules: vec![],
            start: start.to_owned(),
            predicates: preds.into_iter().collect(),
        };
        for r in rules {
            let idx = rule_counter.get(&r.0).unwrap();
            g.rules.push(Rule {
                lhs: r.0.to_owned(),
                idx: *idx,
                rhs: r.1,
            });
            rule_counter.insert(r.0, idx + 1);
        }
        return g;
    }

    pub fn nullable_rules<'a>(&'a self) -> HashSet<&'a Rule<T, N, PN>> {
        let mut nullable_rules: HashSet<&Rule<T, N, PN>> = HashSet::new();
        let mut nullable_nonterms: HashSet<&N> = HashSet::new();
        let mut change = true;
        while change {
            change = false;
            for r in &self.rules {
                if !nullable_rules.contains(r)
                    && r.rhs.iter().all(|s| match s {
                        Node::NonTerm(name) => nullable_nonterms.contains(name),
                        Node::Terminal(_) => false,
                        Node::PredTerm(_) => false,
                    })
                {
                    nullable_rules.insert(r);
                    nullable_nonterms.insert(&r.lhs);
                    change = true;
                }
            }
        }
        return nullable_rules;
    }
}
